﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMapper
{
    public class Patient
    {
        public int PatientId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Job { get; set; }
        public string FatherName { get; set; }
        public string salary { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }

    }
}
