﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoMapper
{
    public class Users
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string company { get; set; }
        public string FatherName { get; set; }
        public string Dept { get; set; }
        public string Gender { get; set; }
        public string MaritialStatus { get; set; }

    }
    public class Patient
    {
        public int PatientId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Job { get; set; }
        public string FatherName { get; set; }
        public string salary { get; set; }
        public string Gender { get; set; }
        public string MaritialStatus { get; set; }

    }


    public class Demo
    {

        static void Main(string[] args)
        {
            Users o = new Users();
            o.Id = 100;
            o.Name = "Niraj";
            o.Email = "njmdr888@gmail.com";
            o.Address = "Balaju";
            o.company = "Code IT";
            o.FatherName = "Krishna";
            o.Dept = "CS";
            o.Gender = "Male";
            o.MaritialStatus = "Single";

            //Old configuration of automapper:
            //AutoMapper.Mapper.CreateMap<User, Patient>();


            //New configuration of automapper:
           Mapper.Initialize(cfg =>
                {
                    cfg.CreateMap<Users, Patient>()
                    .ForMember(dest => dest.PatientId, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.Job, opt => opt.MapFrom(src => src.company))
                    .ForMember(dest => dest.salary, opt => opt.MapFrom(src => src.Dept));
                });

            var mapdata = AutoMapper.Mapper.Map<Users, Patient>(o);

            Console.WriteLine(mapdata.PatientId + Environment.NewLine + mapdata.Name + Environment.NewLine + mapdata.Email + Environment.NewLine + mapdata.Address + Environment.NewLine + mapdata.Job + Environment.NewLine + mapdata.FatherName + Environment.NewLine + mapdata.salary + Environment.NewLine + mapdata.Gender + Environment.NewLine + mapdata.MaritialStatus);

            Console.ReadLine();



        }
    }
}
