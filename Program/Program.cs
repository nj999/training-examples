﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            //IOC ioc = new IOC();
            //Console.WriteLine(ioc.GetMyCountryName());
            //Console.ReadLine();

            WorkInProgress workInProgress = new WorkInProgress(new IOCUsa());

            Console.WriteLine(workInProgress.GetMyCountryName());

            Console.ReadLine();
        }

        private static IOCService IOCUsa()
        {
            throw new NotImplementedException();
        }


    }

    public class WorkInProgress
    {
        private readonly IOCService _oCService;

        public WorkInProgress(IOCService oCService)
        {
            this._oCService = oCService;
        }

        public string GetMyCountryName()
        {
            return _oCService.GetMyCountryName();
        }
    }
}
