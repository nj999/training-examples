﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    public interface IOCService
    {
        string GetMyCountryName();
    }

    public class IOC : IOCService
    {
        public string GetMyCountryName()
        {
            return "Nepal";
        }
    }


    public class IOCUsa : IOCService
    {
        public string GetMyCountryName()
        {
            return "USA";
        }
    }
}